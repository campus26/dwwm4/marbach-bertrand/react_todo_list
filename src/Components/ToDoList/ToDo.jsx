import "./ToDo.css";

export function Task({ status, taskText, onClick }) {
  let ClickStatusIcon = "";
  let toDoSpecificClickIcon = '';
  let toDoSpecificTaskText = '';

  switch (status) {
    case 'Open':
      ClickStatusIcon = '☐';
      toDoSpecificClickIcon = 'toDoClickStatusIcon';
      toDoSpecificTaskText = 'toDoTaskText';
      break;
    case 'Procrastinate':
      ClickStatusIcon = '🏖️';
      toDoSpecificClickIcon = 'toDoProcrastinateIcon';
      toDoSpecificTaskText = 'toDoProcrastinateText';
      break;
    case 'Done':
      ClickStatusIcon = '☑️';
      toDoSpecificClickIcon = 'toDoDoneIcon';
      toDoSpecificTaskText = 'toDoDoneText';
      break;
    default:
      break;
  }

  return (
    <div className="thisTask" onClick={onClick}>
      <div className={`toDoKmon toDoKmonClickIcon ${toDoSpecificClickIcon}`}>{ClickStatusIcon}</div>
      <div className={`toDoKmon toDoKmonTask ${toDoSpecificTaskText}`}>{taskText}</div>
    </div>
  );
}
