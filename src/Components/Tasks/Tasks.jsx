import { Task } from "../ToDoList/ToDo";

export function Tasks({ data, onStatusClick }) {
  console.log(data);

  return (
    <>
      {data.map((task) => (
        <Task key={task.id} status={task.status} taskText={task.taskText} onClick={() => onStatusClick(task.id)} />
      ))}
    </>
  );
}
