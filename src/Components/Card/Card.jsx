
import "./Card.css"

export function Card({title, image}) {

    return (
        <div className="card">
         <img src={image} alt={title} width="200" height="250"/>
         <h3>{title}</h3>
         <div>Ci-dessus l'objectif !</div>
        </div>
    );
}