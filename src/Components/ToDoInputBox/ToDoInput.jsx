import {useState} from 'react';
import "./ToDoInput.css";

export function ToDoInput({inputType, inputDefaultText, buttonText, onAddTask}) {
    const [newTask, setNewTask] = useState('');


    const handleButtonClick = () => {
        console.log ("Button clicked");
        if (newTask.trim() !== '') {
            console.log ("Adding Task :", newTask);
            onAddTask(newTask);
            setNewTask('');
        } else {
            console.log ('Task is empty');
        }
    };

    console.log ("Rendering ToDoInput");

    return (
        <div className="toDoInput">
            <input 
            type={inputType} 
            placeholder={inputDefaultText}
            value={newTask}
            onChange= {(e) => setNewTask(e.target.value)}/>
        <button onClick={handleButtonClick} type="button">
            <span>{buttonText}</span></button>
        </div>
    );
}