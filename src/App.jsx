import { useState, useEffect } from 'react';
import { Card } from './Components/Card/Card';
import { ToDoInput } from './Components/ToDoInputBox/ToDoInput';
import { Tasks } from './Components/Tasks/Tasks';

function App() {
  const [tasks, setTasks] = useState([]);

  useEffect(() => {
    const storedTasks = JSON.parse(localStorage.getItem('tasks')) || [];
    setTasks(storedTasks);
  }, []);

  const handleAddTask = (newTask) => {
    const timeStamp = new Date().getTime();
    const updatedTasks = [...tasks, { id: timeStamp, status: 'Open', taskText: newTask }];
    setTasks(updatedTasks);
    localStorage.setItem('tasks', JSON.stringify(updatedTasks));
  };

  const handleStatusClick = (taskId) => {
    const updatedTasks = tasks.map((task) => {
      if (task.id === taskId) {
        switch (task.status) {
          case 'Open':
            return { ...task, status: 'Done' };
          case 'Done':
            return { ...task, status: 'Procrastinate' };
          case 'Procrastinate':
            return { ...task, status: 'Open' };
          default:
            return task;
        }
      }
      return task;
    });

    setTasks(updatedTasks);
    localStorage.setItem('tasks', JSON.stringify(updatedTasks));
  };

  return (
    <>
      <div className="appFlex">
        <Card title="ToDo list template" image="src/Components/Card/assets/img/ToDoImg.png" />

        <div className="toDoTitle">
          <div className="toDoTitleLeft">
            <h2>Todo</h2>
          </div>
          <div className="toDoTitleRight">
            <h5> Today 📅</h5>
          </div>
        </div>

        <ToDoInput inputType="text" inputDefaultText="Add a new task" buttonText="+" onAddTask={handleAddTask} />

        <Tasks data={tasks} onStatusClick={handleStatusClick} />
      </div>
    </>
  );
}

export default App;
